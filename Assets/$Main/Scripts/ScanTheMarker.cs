﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanTheMarker : MonoBehaviour
{
    [SerializeField] MarkerDetection markerDetection;
    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (ARManager.Instance.isArActive)
        {
            GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.IntroAnimation);
        }
    }
    public void OnClickHere()
    {
        markerDetection.isDetected = true;
    }
}
