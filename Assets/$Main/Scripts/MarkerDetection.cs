﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerDetection : MonoBehaviour
{
    internal bool isDetected;
    bool isFirstTime = true;

    private void OnEnable() 
    {
        if(!isFirstTime)
        {
        isDetected = true;
        Debug.Log("OnEnable");
        }
        else
        {
            isFirstTime = false;
        }
    }

    private void OnDisable() 
    {
        isDetected = false;
        Debug.Log("OnDisable");
    }
}
