﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using easyar;

public class ARManager : MonoBehaviour
{
    public static ARManager Instance;
    public GameObject target;
    public Transform camera;
    [SerializeField] MarkerDetection markerDetection;
    public bool isArActive;
    Vector3 initialPos = Vector3.zero;
    Vector3 initialRot = Vector3.zero;
    public void Awake()
    {
        Instance = this;
        initialPos = camera.position;
        initialRot = camera.eulerAngles;
    }
    void Update()
    {
        if (!isArActive && markerDetection.isDetected)
        {
            OnDetected();
        }
        else if (isArActive)
        {
            OnDetectionLost();
        }
    }
    void OnDetected()
    {
        this.target.SetActive(true);
        isArActive = true;
    }
    void OnDetectionLost()
    {
        isArActive = false;
        camera.position = initialPos;
        camera.eulerAngles = initialRot;
    }
}
