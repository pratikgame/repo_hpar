﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class TextAnimMicroBezel : MonoBehaviour
{
    public TextMeshProUGUI text;
    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(PlayAnim());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    IEnumerator PlayAnim()
    {
        float time = 0f;
        float animTime = 2.0f;

        while (time / animTime < 1.0f)
        {
            text.text = "<size=112><b>" + (int)(90f * time / animTime) + "+%</b></size>\nScreen-to-Body Ratio";
            time += Time.deltaTime;
            yield return null;
        }

        text.text = "<size=112><b>90+%</b></size>\nScreen-to-Body Ratio";

        yield break;
    }
}
