﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTheOrienntation : MonoBehaviour
{
    [SerializeField] GameObject laptop;
    public void OnClickProceed()
    {
        laptop.SetActive(true);
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.ScanTheMarker);
    }
}
