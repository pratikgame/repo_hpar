﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;


public class WifiAndLTE : MonoBehaviour
{
    public Text animatedTxt;
    public Toggle tglLte;
    bool isLTE = false;

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(PlayFakeDownloading());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        TargetManager.Instance.StopVideoOnLaptopScreen();
        TargetManager.Instance.ResetLaptopScreen();
    }

    IEnumerator PlayFakeDownloading()
    {
        tglLte.isOn = false;
        isLTE = false;
        TargetManager.Instance.StopVideoOnLaptopScreen();
        animatedTxt.text = "Stay connected seamlessly";
        float time = 0.0f;
        float seconds = 10.0f;
        TargetManager.Instance.PlayVideoOnLaptopScreen();


        do
        {
            while (time / seconds < 1.0f && !isLTE)
            {
                time += Time.deltaTime;
                if (time >= 5)
                    animatedTxt.text = "with our built-in dual connectivity feature";

                yield return null;
            }
            if (!isLTE)
            {
                time = 7.0f;
                TargetManager.Instance.LoopVideoOnLaptopScreen(time);
            }
        }
        while (!isLTE);
        yield return new WaitUntil(() => isLTE);
        float resumeFrom = 11;
        animatedTxt.text = "which runs at the same time";
        do
        {
            time = 0;
            seconds = 9f;
            TargetManager.Instance.ResumeVideoOnLaptopScreen(resumeFrom);
            resumeFrom = 12;
            while (time / seconds < 1.0f)
            {
                if (!isLTE)
                {
                    TargetManager.Instance.PauseVideoOnLaptopScreen();
                    yield return new WaitUntil(() => isLTE);
                    TargetManager.Instance.NormlResumeVideoOnLaptopScreen();
                    continue;
                }
                time += Time.deltaTime;
                if (time >= 2.0f && time <= 3.0f)
                {
                    animatedTxt.text = "with no connectivity lags.";
                }
                yield return null;
            }

            // TargetManager.Instance.ResumeVideoOnLaptopScreen();
            // yield return new WaitForSeconds(2.0f);
            // animatedTxt.text = "to ensure no\nconnectivity\nlags";
            // yield return new WaitForSeconds(7.0f);
        }
        while (true);

        yield break;
    }

    public void OnClickHome()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu);
    }

    public void OnClickClose()
    {

    }

    public void OnToggleLTE(bool isOn)
    {
        isLTE = isOn;
    }
}
