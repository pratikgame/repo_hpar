﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public static AnimationHandler Instance;

    public enum AnimationClip { Animation_1, Animation_2, Animation_3, Animation_4, Animation_5 };
    Animation animation;
    AnimationClip currClip = AnimationClip.Animation_1;
    internal bool playAllAnimationInSequence = false;
    void Awake()
    {
        Instance = this;
        animation = GetComponent<Animation>();
    }

    internal void Play(AnimationClip clip)
    {
        animation.Play(clip.ToString());
        currClip = clip;
    }

    internal void Play()
    {
        playAllAnimationInSequence = true;
        Play(AnimationClip.Animation_3);
    }

    void Update()
    {
        if (playAllAnimationInSequence)
        {
            if (!animation.isPlaying)
            {
                if ((int)currClip < (int)AnimationClip.Animation_5)
                {
                    currClip = (AnimationClip)((int)currClip + 1);
                    Play(currClip);
                }
                else
                {
                    playAllAnimationInSequence = false;
                }
            }
        }
    }
}
