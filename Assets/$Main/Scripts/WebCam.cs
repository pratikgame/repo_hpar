﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCam : MonoBehaviour
{
    bool isWebCamOn = false;
    [SerializeField] Toggle toggle;
    [SerializeField] GameObject killSwitchTextAnimGO;
    [SerializeField] GameObject killSwitchAnimGO;
    [SerializeField] GameObject killSwitchActualObject;

    Vector3 originalPos = Vector3.zero;
    Vector3 originalScale = Vector3.zero;

    private void OnEnable()
    {
        toggle.isOn = false;
        isWebCamOn = false;
        StopAllCoroutines();
        StartCoroutine(PlaySequence());
    }
    private void OnDisable()
    {
        TargetManager.Instance.transform.position = originalPos;
        TargetManager.Instance.transform.localScale = originalScale;
        TargetManager.Instance.RotateObject(0);
        TargetManager.Instance.ResetLaptopScreen();
        toggle.onValueChanged.RemoveListener(OnToggleWebCam);
    }
    public void OnToggleWebCam(bool isOn)
    {
        isWebCamOn = isOn;
        Reset();
    }
    public void Reset()
    {
        if (isWebCamOn)
            TargetManager.Instance.ShowVideoCallOnLaptopScreen();
        else
            TargetManager.Instance.ShowVideoCallWhenWebCamIsOffOnLaptopScreen();
    }
    public void OnClickHome()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu);
    }
    public void OnClickClose()
    {

    }
    IEnumerator PlaySequence()
    {
        originalPos = TargetManager.Instance.transform.position;
        originalScale = TargetManager.Instance.transform.localScale;

        Vector3 targetPos = new Vector3(-1.1f, -0.31f, 0.16f);
        Vector3 trgetScale = new Vector3(6f, 6f, 6f) - originalScale;

        TargetManager.Instance.ShowVideoCallOnLaptopScreen();
        toggle.gameObject.SetActive(false);
        killSwitchTextAnimGO.SetActive(false);
        killSwitchAnimGO.SetActive(false);
        killSwitchActualObject.SetActive(false);
        killSwitchAnimGO.transform.localScale = new Vector3(1f, 1f, 1f);

        // yield return new WaitForSeconds(2.0f);
        float time = 0;
        // killSwitchTextAnimGO.SetActive(true);
        // killSwitchTextAnimGO.GetComponentInChildren<Text>().text = "Webcam\nKillswitch";

        while (time / 2.0f < 1.0f)
        {
            TargetManager.Instance.transform.position = targetPos * time / 2.0f;
            TargetManager.Instance.transform.localScale = originalScale + trgetScale * time / 2.0f;
            TargetManager.Instance.RotateObject(60 * time / 2.0f);
            time += Time.deltaTime * 2.0f;
            yield return null;
        }

        killSwitchTextAnimGO.SetActive(true);
        killSwitchTextAnimGO.GetComponentInChildren<Text>().text = "Work conveniently anywhere";

        // yield return new WaitForSeconds(2.0f);
        // killSwitchTextAnimGO.SetActive(false);
        killSwitchAnimGO.SetActive(true);
        toggle.gameObject.SetActive(true);
        toggle.transform.Find("Image").gameObject.SetActive(true);
        toggle.onValueChanged.AddListener(OnToggleWebCam);
        toggle.isOn = true;
        time = 0.0f;
        while (time / 2.0f < 1.0f)
        {
            killSwitchAnimGO.transform.localScale = Vector3.one + new Vector3(1f, 1f, 1f) * time / 2.0f;
            time += Time.deltaTime;
            yield return null;
        }

        yield return new WaitUntil(() => !isWebCamOn);
        toggle.transform.Find("Image").gameObject.SetActive(false);
        killSwitchAnimGO.SetActive(false);
        killSwitchActualObject.SetActive(true);
        // killSwitchTextAnimGO.SetActive(true);
        // killSwitchTextAnimGO.GetComponentInChildren<Text>().text = "Work conveniently anywhere";

        yield return new WaitForSeconds(0.0f);
        killSwitchTextAnimGO.GetComponentInChildren<Text>().text = "with our webcam kill switch feature";

        yield return new WaitForSeconds(5.0f);
        killSwitchTextAnimGO.GetComponentInChildren<Text>().text = "by switching on the button.";

        yield break;
    }
}
