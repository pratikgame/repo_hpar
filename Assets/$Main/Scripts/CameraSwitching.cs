﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitching : MonoBehaviour
{
    bool isTriggered;
    [SerializeField] GameObject _Target;
    [SerializeField] GameObject _StaticPosGO;
    [SerializeField] GameObject _TargetOriginalParent;
    [SerializeField] GameObject _TargetCamera;
    // Start is called before the first frame update
    private void OnEnable()
    {
        _TargetCamera.transform.parent = null;
        _Target.transform.SetParent(_TargetOriginalParent.transform);
    }

    private void OnDisable()
    {
        if (isTriggered)
        {
            _Target.transform.parent = null;

            _TargetCamera.transform.SetParent(_StaticPosGO.transform);
            _TargetCamera.transform.localPosition = Vector3.zero;
            _TargetCamera.transform.localRotation = Quaternion.identity;
        }
        else
            isTriggered = true;


    }
}
