﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroEdgeBezelEdgeScreenAnim : MonoBehaviour
{
    [SerializeField] MicroEdgeBezelPinchZoomAnim microEdgeBezelPinchZoom;
    Vector3 originalScale;
    private void Awake()
    {
        originalScale = transform.localScale;
    }
    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(PlayAnim());
    }
    void OnDisable()
    {
        StopAllCoroutines();
    }
    IEnumerator PlayAnim()
    {
        Vector3 currScale = originalScale;
        currScale.x = 0f;
        float time = 0f;
        float animTime = 4.0f;
        transform.localScale = currScale;

        while (time / animTime < 1.0f)
        {
            currScale.x = Mathf.Lerp(0, originalScale.x, time / animTime);
            transform.localScale = currScale;
            time += Time.deltaTime;
            yield return null;
        }

        transform.localScale = originalScale;

        yield break;
    }
}
