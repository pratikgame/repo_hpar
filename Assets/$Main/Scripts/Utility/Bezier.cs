﻿using UnityEngine;
using System.Collections.Generic;
[RequireComponent(typeof(LineRenderer))]
public static class Bezier
{   
    public static List<Vector3> DrawCurve(Vector3[] controlPoints)
    {
		int curveCount = (int)controlPoints.Length / 3;
		int SEGMENT_COUNT = 50;
		List<Vector3> path = new List<Vector3>();
		path.Add(controlPoints[0]);

        for (int j = 0; j <curveCount; j++)
        {
            for (int i = 1; i <= SEGMENT_COUNT; i++)
            {
                float t = i / (float)SEGMENT_COUNT;
                int nodeIndex = j * 3;
                Vector3 pixel = CalculateCubicBezierPoint(t, controlPoints [nodeIndex], controlPoints [nodeIndex + 1], controlPoints [nodeIndex + 2], controlPoints [nodeIndex + 3]);
                path.Add(pixel);
				// lineRenderer.SetVertexCount(((j * SEGMENT_COUNT) + i));
                // lineRenderer.SetPosition((j * SEGMENT_COUNT) + (i - 1), pixel);
            }
            
        }

		return path;
    }

	public static List<Vector3> DrawQardicCurve(Vector3[] controlPoints)
    {
		int curveCount = (int)controlPoints.Length / 3;
		int SEGMENT_COUNT = 50;
		List<Vector3> path = new List<Vector3>();
		path.Add(controlPoints[0]);

        for (int j = 0; j <curveCount; j++)
        {
            for (int i = 1; i <= SEGMENT_COUNT; i++)
            {
                float t = i / (float)SEGMENT_COUNT;
                int nodeIndex = j * 3;
                Vector3 pixel = CalculateQuardicBezierPoint(t, controlPoints [nodeIndex], controlPoints [nodeIndex + 1], controlPoints [nodeIndex + 2]);
                path.Add(pixel);
				// lineRenderer.SetVertexCount(((j * SEGMENT_COUNT) + i));
                // lineRenderer.SetPosition((j * SEGMENT_COUNT) + (i - 1), pixel);
            }
            
        }

		return path;
    }
        
    static Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
        
        Vector3 p = uuu * p0; 
        p += 3 * uu * t * p1; 
        p += 3 * u * tt * p2; 
        p += ttt * p3; 
        
        return p;
    }

	static Vector3 CalculateQuardicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        // B(t) = (1-t)2P0 + 2(1-t)tP1 + t2P2 , 0 < t < 1
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
		
        Vector3 p = uu * p0; 
        p += 2 * u * t * p1; 
        p +=  tt * p2; 
		
        return p;
    }
}