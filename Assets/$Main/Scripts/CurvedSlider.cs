﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Events;

public class CurvedSlider : MonoBehaviour
{
    public UnityAction<float> onSliderValueChanged;
    public RectTransform[] curvePoints;
    public RectTransform slider;
    public float percentage = 0.5f;
    bool followThePath = false;
    float smoothness = 20.0f;
    // Start is called before the first frame update
    List<Vector3> movementPoints;
    Vector2 originalPos;
    private void Awake()
    {
        originalPos = slider.anchoredPosition;
    }
    public void Start()
    {


        movementPoints = Bezier.DrawQardicCurve(new Vector3[]
        {
            curvePoints[0].position, curvePoints[1].position, curvePoints[2].position, curvePoints[2].position, curvePoints[3].position, curvePoints[4].position });
    }

    private void OnEnable()
    {
        slider.anchoredPosition = originalPos;
        percentage = 0.5f;
    }

    public void OnPointerDown()
    {
        StopAllCoroutines();
        followThePath = true;
        StartCoroutine(FollowThePath());
    }

    public void OnPointerUp()
    {
        StopAllCoroutines();
        followThePath = false;
    }

    IEnumerator FollowThePath()
    {
        while (followThePath)
        {
            Vector2 screenPos;
#if !UNITY_EDITOR
			screenPos = Input.touches[0].position;
#else
            screenPos = Input.mousePosition;
#endif

            Vector2 targetPos = screenPos;
            // RectTransformUtility.ScreenPointToLocalPointInRectangle(sliderRect,screenPos,AppController.GetInstance().MainCamera,out targetPos);

            float perc = (movementPoints[0].x - targetPos.x) / (movementPoints[0].x - movementPoints[movementPoints.Count - 1].x);
            bool isValueChanged = false;
            if (perc != percentage)
                isValueChanged = true;

            percentage = Mathf.Clamp(perc, 0.0f, 1.0f);

            if (isValueChanged && onSliderValueChanged != null)
                onSliderValueChanged.Invoke(percentage);

            // Debug.Log("percentage:"+percentage+" targetPos:"+targetPos +"First:"+(pathForThumbMovement[0].x - targetPos.x)+"Second:"+Mathf.Abs(pathForThumbMovement[0].x - pathForThumbMovement[pathForThumbMovement.Count - 1].x));

            targetPos = movementPoints[(int)((float)(movementPoints.Count - 1) * percentage)];
            slider.position = Vector2.Lerp(slider.position, targetPos, Time.deltaTime * smoothness);

            yield return null;
        }
        yield break;
    }
}
