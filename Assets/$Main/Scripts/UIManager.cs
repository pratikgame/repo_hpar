﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    public UnityAction onBackKey;
    public enum Screen
    {
        ChangeOrientation, ScanTheMarker, MainMenu, IntroAnimation, BatteryInfo, PrivacyScreen, MicroEdgeBezel, View360, WifiAndLTE, WebCam, End
    };
    public Screen currentScreen = Screen.ChangeOrientation;
    public Dictionary<Screen, GameObject> dictScreen;
    public Dictionary<Screen, Screen> backScreen;
    private void Awake()
    {
        LoadReferences();
    }
    private void LoadReferences()
    {
        dictScreen = new Dictionary<Screen, GameObject>();
        backScreen = new Dictionary<Screen, Screen>();
        for (int i = 0; i < (int)Screen.End; i++)
        {
            dictScreen.Add((Screen)i, transform.Find(((Screen)i).ToString()).gameObject);
            backScreen.Add((Screen)i, (Screen)i);
        }
        ChangeScreen(currentScreen);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HandleBackKey();
        }
    }
    public void HandleBackKey()
    {
        if (onBackKey != null)
        {
            onBackKey.Invoke();
        }
    }
    public void ChangeScreen(Screen nextScreen, bool isBack = false)
    {
        if (!isBack)
        {
            backScreen[nextScreen] = currentScreen;
        }
        dictScreen[currentScreen].SetActive(false);
        currentScreen = nextScreen;

        dictScreen[nextScreen].SetActive(true);

    }
}