﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View360 : MonoBehaviour
{
    private void OnEnable()
    {

    }
    private void OnDisable()
    {

    }

    public void OnClickHome()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu, true);
    }

    public void OnClickClose()
    {

    }
}
