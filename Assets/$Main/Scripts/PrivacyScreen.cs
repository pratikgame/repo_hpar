﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivacyScreen : MonoBehaviour
{
    public CurvedSlider curvedSlider;
    public Text animatedText;

    void OnEnable()
    {
        TargetManager.Instance.SetActivePrivacyOverLay(false);
        curvedSlider.onSliderValueChanged += OnSliderValueChanged;
        StopAllCoroutines();
        StartCoroutine(PlayTextAnimation());
    }

    void OnSliderValueChanged(float value)
    {
        TargetManager.Instance.RotateObject(45f - 90 * value);
        if (value > 0.8f || value < 0.2f)
            TargetManager.Instance.SetActivePrivacyOverLay(true);
        else
            TargetManager.Instance.SetActivePrivacyOverLay(false);

    }

    void OnDisable()
    {
        curvedSlider.onSliderValueChanged -= OnSliderValueChanged;
        TargetManager.Instance.RotateObject(0f);
        TargetManager.Instance.SetActivePrivacyOverLay(false);
    }

    public void OnClickHome()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu, true);
    }

    public void OnClickClose()
    {

    }

    public IEnumerator PlayTextAnimation()
    {
        animatedText.text = "Protect your personal data";
        yield return new WaitForSeconds(1.5f);
        animatedText.text = "by hitting the “F1” button";
        yield return new WaitForSeconds(1.5f);
        animatedText.text = "to activate the privacy screen";
    }
}
