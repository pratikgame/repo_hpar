﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;

public class BatteryInfo : MonoBehaviour
{
    public Animator sunAnim;
    public Animator moonAnim;
    public GameObject batteryProgress;
    public Text textAnim;
    bool sunAnimFlag = true;
    int animCnt = 0;


    Vector3 originalScale;

    void Awake()
    {
        originalScale = batteryProgress.transform.localScale;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    void OnEnable()
    {
        // sunAnimFlag = true;
        animCnt = 0;
        // sunAnim.gameObject.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(DrainBattery());
        TargetManager.Instance.PlayBatteryVid();
    }
    private void OnDisable()
    {
        TargetManager.Instance.StopBatteryVid();
    }

    public void Home()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu);
    }

    // Update is called once per frame
    // void Update()
    // {

    //     if (sunAnimFlag)
    //     {
    //         if (sunAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
    //         {
    //             // sunAnim.Play();
    //             animCnt++;
    //             if (animCnt == 1)
    //             {
    //                 animCnt = 0;
    //                 sunAnimFlag = false;
    //                 sunAnim.gameObject.SetActive(false);
    //                 moonAnim.gameObject.SetActive(true);
    //             }
    //         }
    //     }
    //     else
    //     {
    //         if (moonAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
    //         {
    //             animCnt++;
    //             if (animCnt == 1)
    //             {
    //                 sunAnim.gameObject.SetActive(false);
    //                 moonAnim.gameObject.SetActive(false);
    //                 GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu);
    //             }
    //         }
    //     }
    // }

    IEnumerator DrainBattery()
    {
        textAnim.text = "With a long battery life of up to 22 hours,";
        bool newtextSetFlag = false;
        batteryProgress.transform.localScale = Vector3.one;
        originalScale = Vector3.one;
        float time = 0;
        float seconds = 40f;
        while (time / seconds < 1.0f)
        {
            originalScale.y = Mathf.Lerp(1.0f, 0.0f, time / seconds);
            batteryProgress.transform.localScale = originalScale;
            time += Time.deltaTime;
            if (!newtextSetFlag && time / seconds >= 0.1f)
            {
                textAnim.text = "you can work and play non-stop.";
                newtextSetFlag = true;
            }
            yield return null;
        }
    }
}
