﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MicroEdgeBezel : MonoBehaviour
{

    [SerializeField] Text _txtAnim;
    [SerializeField] GameObject _anim2GO;
    [SerializeField] Text _txtAnim2;
    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(PlayAnim());
        // TargetManager.Instance.ActivateMicroEdgeBezelEdgeScreenAnim();
        // TargetManager.Instance.ActivateMicroEdgeBezelPinchZoomAnim();
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        TargetManager.Instance.DeActivateMicroEdgeBezelEdgeScreenAnim();
        TargetManager.Instance.DeActivateMicroEdgeBezelPinchZoomAnim();
    }
    // Start is called before the first frame update
    public void OnClickHome()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu, true);
    }
    public void OnClickClose()
    {

    }

    IEnumerator PlayAnim()
    {
        _anim2GO.SetActive(false);
        _txtAnim2.gameObject.SetActive(false);
        _txtAnim.text = "Enjoy near borderless view";
        MicroEdgeBezelPinchZoomAnim micro = TargetManager.Instance._MicroEdgeBezelPinchZoomAnim.GetComponent<MicroEdgeBezelPinchZoomAnim>();

        TargetManager.Instance.ActivateMicroEdgeBezelPinchZoomAnim();
        yield return new WaitUntil(() => micro.pinchZoom);
        // yield return new WaitForSeconds(3.0f);
        TargetManager.Instance.DeActivateMicroEdgeBezelPinchZoomAnim();
        TargetManager.Instance.ActivateMicroEdgeBezelEdgeScreenAnim();
        _anim2GO.SetActive(true);
        _txtAnim2.gameObject.SetActive(true);
        _txtAnim.text = "on your\nscreen from\nevery angle";
        float time = 0;
        float animTime = 3.5f;

        while (time / animTime <= 1.0f)
        {
            _txtAnim2.text = "<b><size=72>" + ((int)(90f * time / animTime)) + "+%</size></b>\nScreen-to-Body\nRatio";
            time += Time.deltaTime;
            yield return null;
        }
        _txtAnim2.text = "<b><size=72>90+%</size></b>\nScreen-to-Body\nRatio";
        yield return null;
    }
}
