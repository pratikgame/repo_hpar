﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroAnimation : MonoBehaviour
{
    void OnEnable()
    {
        AnimationHandler.Instance.Play();
    }

    void Update()
    {
        if (!AnimationHandler.Instance.playAllAnimationInSequence)
        {
            GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MainMenu);
        }
    }
}
