﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroEdgeBezelPinchZoomAnim : MonoBehaviour
{
    [SerializeField] Renderer render;
    Vector2 targetScale = new Vector2(0.5f, 0.5f);
    internal bool pinchZoom;

    private void OnEnable()
    {
        pinchZoom = false;
        StopAllCoroutines();
        StartCoroutine(PlayAnim());
    }
    private void OnDisable()
    {
        pinchZoom = false;
        render.material.mainTextureScale = new Vector2(1f, 1f);
        StopAllCoroutines();
    }

    IEnumerator PlayAnim()
    {
        Vector2 touch1 = Vector2.zero;
        Vector2 touch2 = Vector2.zero;
        bool reset = true;
        float initalDistance = 0f;
        float currDistance = 0f;
        float zoomSpeed = 0.05f;

#if !UNITY_EDITOR
        while (Vector2.Distance(render.material.mainTextureScale, targetScale) >= 0.1f)
        {
            if (Input.touchCount == 2)
            {
                touch1 = Input.touches[0].position;
                touch2 = Input.touches[1].position;
                if (reset)
                {
                    initalDistance = Vector2.Distance(touch1, touch2); ;
                    reset = false;
                }

                currDistance = Vector2.Distance(touch1, touch2);
                float diff = currDistance - initalDistance;
                diff = Mathf.Clamp(render.material.mainTextureScale.x - zoomSpeed * diff * Time.deltaTime, 0.5f, 1.0f);
                render.material.mainTextureScale = new Vector2(diff, diff);
                initalDistance = currDistance;

            }
            else
                reset = true;

            yield return null;
        }
#endif
        // float time = 0.0f;
        // float animTime = 2.0f;

        // while (time / animTime < 1.0f)
        // {
        //     time += Time.deltaTime;
        //     
        //     yield return null;
        // }
        yield return new WaitForSeconds(2.0f);
        pinchZoom = true;
        yield return null;
    }

}
