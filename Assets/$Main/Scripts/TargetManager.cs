﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class TargetManager : MonoBehaviour
{
    [SerializeField] GameObject _3DCanvas;
    [SerializeField] GameObject _Target;
    [SerializeField] GameObject _MicroEdgeBezelEdgeSceenAnim;
    [SerializeField] internal GameObject _MicroEdgeBezelPinchZoomAnim;
    [SerializeField] GameObject _BatteryAnimMoon;
    [SerializeField] GameObject _BatteryAnimSun;
    [SerializeField] GameObject _batteryVid;
    [SerializeField] GameObject _LaptopScreen;
    [SerializeField] GameObject _PrivacyOverLay;
    [SerializeField] GameObject _VideoOverLay;
    [SerializeField] Material _videoMaterialForLaptopScreen;
    [SerializeField] Material _staticScreenMaterialForLaptopScreen;
    [SerializeField] Material _staticScreenVideoCallMaterialForLaptopScreen;
    [SerializeField] Material _staticScreenWebCamOffVideoCallMaterialForLaptopScreen;

    internal UnityAction onClickBatteryFeature;
    internal UnityAction onClickPrivacyScreenFeature;
    internal UnityAction onClickMicroEdgeBezelFeature;
    internal UnityAction onClickWifiAndLTEFeature;
    internal UnityAction onClickWebCamFeature;
    public static TargetManager Instance;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void OnClickFeatureBatteryInfo()
    {
        if (onClickBatteryFeature != null)
            onClickBatteryFeature();
    }

    public void OnClickPrivacyScreenInfo()
    {
        if (onClickPrivacyScreenFeature != null)
        {
            onClickPrivacyScreenFeature();
        }
    }

    public void OnClickMicroEdgeBezel()
    {
        if (onClickMicroEdgeBezelFeature != null)
        {
            onClickMicroEdgeBezelFeature();
        }
    }

    public void OnClickWifiAndLTE()
    {
        if (onClickWifiAndLTEFeature != null)
        {
            onClickWifiAndLTEFeature();
        }
    }

    public void OnClickWebCam()
    {
        if (onClickWebCamFeature != null)
        {
            onClickWebCamFeature();
        }
    }

    public void ShowFeatureOptions()
    {
        _3DCanvas.SetActive(true);
    }

    public void HideFeatureOptions()
    {
        _3DCanvas.SetActive(false);
    }

    public void RotateObject(float angle)
    {
        _Target.transform.localEulerAngles = new Vector3(0f, angle, 0f);
    }

    public void ActivateMicroEdgeBezelEdgeScreenAnim()
    {
        _MicroEdgeBezelEdgeSceenAnim.SetActive(true);
    }

    public void DeActivateMicroEdgeBezelEdgeScreenAnim()
    {
        _MicroEdgeBezelEdgeSceenAnim.SetActive(false);
    }

    public void ActivateMicroEdgeBezelPinchZoomAnim()
    {
        _MicroEdgeBezelPinchZoomAnim.SetActive(true);
    }

    public void DeActivateMicroEdgeBezelPinchZoomAnim()
    {
        _MicroEdgeBezelPinchZoomAnim.SetActive(false);
    }

    public void PlayVideoOnLaptopScreen()
    {
        _VideoOverLay.SetActive(true);
        _VideoOverLay.GetComponent<Renderer>().material = _videoMaterialForLaptopScreen;
        _VideoOverLay.GetComponent<VideoPlayer>().enabled = true;
        _VideoOverLay.GetComponent<VideoPlayer>().Play();
    }

    public void PauseVideoOnLaptopScreen()
    {
        _VideoOverLay.GetComponent<VideoPlayer>().Pause();
    }

    public void LoopVideoOnLaptopScreen(float fromTime)
    {
        _VideoOverLay.GetComponent<VideoPlayer>().time = fromTime;
    }

    public void NormlResumeVideoOnLaptopScreen()
    {
        _VideoOverLay.GetComponent<VideoPlayer>().Play();
    }
    public void ResumeVideoOnLaptopScreen(float resumeFrom = 11f)
    {
        _VideoOverLay.GetComponent<VideoPlayer>().Play();
        _VideoOverLay.GetComponent<VideoPlayer>().time = resumeFrom;
    }

    public void StopVideoOnLaptopScreen()
    {
        _VideoOverLay.GetComponent<VideoPlayer>().Stop();
        _VideoOverLay.GetComponent<VideoPlayer>().enabled = false;
        _VideoOverLay.SetActive(false);
        ResetLaptopScreen();
    }

    public void ShowVideoCallOnLaptopScreen()
    {
        _LaptopScreen.GetComponent<Renderer>().material = new Material(_staticScreenVideoCallMaterialForLaptopScreen);
    }

    public void ResetLaptopScreen()
    {
        _LaptopScreen.GetComponent<Renderer>().material = new Material(_staticScreenMaterialForLaptopScreen);
    }

    public void ShowVideoCallWhenWebCamIsOffOnLaptopScreen()
    {
        _LaptopScreen.GetComponent<Renderer>().material = new Material(_staticScreenWebCamOffVideoCallMaterialForLaptopScreen);
    }

    public void SetActivePrivacyOverLay(bool flag)
    {
        _PrivacyOverLay.SetActive(flag);
    }

    public void PlayBatteryVid()
    {
        _batteryVid.gameObject.SetActive(true);
        _batteryVid.gameObject.GetComponent<VideoPlayer>().Play();
    }
    public void StopBatteryVid()
    {
        if (_batteryVid.gameObject != null)
        {
            _batteryVid.gameObject.GetComponent<VideoPlayer>().Stop();
            _batteryVid.gameObject.SetActive(false);
        }
    }
}
