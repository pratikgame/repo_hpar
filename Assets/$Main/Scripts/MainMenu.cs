﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    public void OnEnable()
    {
        TargetManager.Instance.ShowFeatureOptions();
        TargetManager.Instance.onClickBatteryFeature += OnClickBattery;
        TargetManager.Instance.onClickPrivacyScreenFeature += OnClickPrivacy;
        TargetManager.Instance.onClickMicroEdgeBezelFeature += OnClickMicroEdge;
        TargetManager.Instance.onClickWifiAndLTEFeature += OnClickWifiAndLTE;
        TargetManager.Instance.onClickWebCamFeature += OnClickWebCam;
    }

    public void OnDisable()
    {
        TargetManager.Instance.HideFeatureOptions();
        TargetManager.Instance.onClickBatteryFeature -= OnClickBattery;
        TargetManager.Instance.onClickPrivacyScreenFeature -= OnClickPrivacy;
        TargetManager.Instance.onClickMicroEdgeBezelFeature -= OnClickMicroEdge;
        TargetManager.Instance.onClickWifiAndLTEFeature -= OnClickWifiAndLTE;
        TargetManager.Instance.onClickWebCamFeature -= OnClickWebCam;
    }

    public void OnClickBattery()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.BatteryInfo);
    }

    public void OnClickPrivacy()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.PrivacyScreen);
    }

    public void OnClickMicroEdge()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.MicroEdgeBezel);
    }

    public void OnClickChangeTheView()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.View360);
    }

    public void OnClickWifiAndLTE()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.WifiAndLTE);
    }

    public void OnClickWebCam()
    {
        GetComponentInParent<UIManager>().ChangeScreen(UIManager.Screen.WebCam);
    }

    public void OnClose()
    {

    }
}
